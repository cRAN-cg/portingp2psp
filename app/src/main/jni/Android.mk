LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := boost_regex
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_regex.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_chrono
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_chrono.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_context
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_context.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_date_time
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_date_time.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_filesystem
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_filesystem.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_exception
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_exception.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_graph
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_graph.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_iostreams
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_iostreams.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_math_c99
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_math_c99.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_math_c99f
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_math_c99f.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_math_c99l
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_math_c99l.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_math_tr1
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_math_tr1.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_math_tr1f
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_math_tr1f.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_math_tr1l
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_math_tr1l.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)


include $(CLEAR_VARS)
LOCAL_MODULE := boost_prg_exec_monitor
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_prg_exec_monitor.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_program_options
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_program_options.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_random
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_random.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_serialization
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_serialization.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_signals
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_signals.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_system
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_system.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_test_exec_monitor
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_test_exec_monitor.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_thread
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_thread.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_timer
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_timer.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_unit_test_framework
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_unit_test_framework.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_wave
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_wave.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boost_wserialization
LOCAL_SRC_FILES := boost_1_51_0/android/lib/libboost_wserialization.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/boost_1_51_0
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE:= PortingBoost
LOCAL_SRC_FILES := PortingBoost.cpp
LOCAL_LDLIBS := -llog
LOCAL_STATIC_LIBRARIES := boost_regex boost_wserialization boost_wave boost_unit_test_framework boost_timer boost_thread boost_test_exec_monitor boost_system boost_signals boost_serialization boost_random boost_program_options boost_prg_exec_monitor boost_math_tr1l boost_math_tr1f boost_math_tr1 boost_math_c99l boost_math_c99f boost_math_c99 boost_iostreams boost_graph boost_filesystem boost_exception boost_date_time boost_context boost_chrono
include $(BUILD_SHARED_LIBRARY)